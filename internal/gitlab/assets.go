package gitlab

// ParseAssets generates an instance of Asset from names and urls
func ParseAssets(assetsLink []string) (*Assets, error) {
	if len(assetsLink) == 0 {
		return nil, nil
	}

	return AssetMarshaller(assetsLink)
}
