package flags

const (
	// ServerURL available in the CLI context
	ServerURL = "server-url"
	// JobToken available in the CLI context
	JobToken = "job-token"
	// PrivateToken available in the CLI context
	PrivateToken = "private-token"
	// ProjectID available in the CLI context
	ProjectID = "project-id"
	// Timeout available in the CLI context
	Timeout = "timeout"
	// Name available in the CLI context
	Name = "name"
	// Description available in the CLI context
	Description = "description"
	// TagName available in the CLI context
	TagName = "tag-name"
	// TagMessage available in the CLI context
	TagMessage = "tag-message"
	// Ref available in the CLI context
	Ref = "ref"
	// AssetsLink available in the CLI context
	AssetsLink = "assets-link"
	// Milestone available in the CLI context
	Milestone = "milestone"
	// ReleasedAt available in the CLI context
	ReleasedAt = "released-at"
	// Debug available in the CLI context
	Debug = "debug"
	// AdditionalCACertBundle available in the CLI context
	AdditionalCACertBundle = "additional-ca-cert-bundle"
	// InsecureHTTPS flag available in the CLI context
	InsecureHTTPS = "insecure-https"
	// IncludeHTMLDescription flag
	IncludeHTMLDescription = "include-html-description"
)
